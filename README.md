# Seminár teórie kategorii pre informatikov

# Zdroje: 

__Osnova__: Jan-Willem Buurlage - [Categories and Haskell](https://github.com/jwbuurlage/category-theory-programmers)
__Odporúčaný__: Bartosz Milewski - [Category Theory for Programmers](https://bartoszmilewski.com/2014/10/28/category-theory-for-programmers-the-preface)
__Doplnkový__: Steve Awodey - [Category Theory](https://englishonlineclub.com/pdf/Category%20Teory%20[EnglishOnlineClub.com].pdf)

# Organizácia: 

Piatok 13:30 v (TBD)

# Témy: 

- 1. Úvod do kategorií, Funktory, Prirodzené transformácie
- 2. Produkty, Koprodukty

TBD
