.PHONY: build

build: 
	mkdir -p build 
	make -C web 
	cp web/index.html build
	cp web/kocicky.css build
	make -C seminars
	cp seminars/*.svg seminars/*.html seminars/*.pdf build
	make -C exercises
	cp exercises/*.html build
sync: build
	rsync -a build/ aisa:public_html/kocicky

all: build


clean: 
	rm -rf build
	make -C exercises clean
	make -C seminars clean 
	make -C web clean
