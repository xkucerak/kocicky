import glob
import os

CAT = """
# Seminár teórie kategórii
```
  |\\      _,,,---,,_
  /,`.-'`'    -.  ;-;;,_
 |,4-  ) )-,_..;\\ (  `'-'
'---''(_/--'  `-'\\_)
```
[domov](index.html)
[cvičenia](cvicenia.html)
"""

HEADER = CAT

for file in sorted(glob.glob("./*.pst")):
    name, ext = os.path.splitext(os.path.basename(file))
    HEADER += f'[{name}]({name}.html)\n'

with open("header.md", "w") as f:
    f.write(HEADER)

